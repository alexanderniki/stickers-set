# Stickers set

Small printable stickers.

Optimized for using with Xiaomi Mi Portable Photo Printer (1040x1560 px).

You can print them and use on your phone or laptop or anywhere else.

Licence: Creative Commons Attribution 4.0 International (CC BY 4.0). See [LICENSE](LICENSE) for details.

Fonts:

- Lexend Zetta (Open Font Licence)
- Poppins (Open Font Licence)
- PT Root UI (Open Font Licence)
- PT Mono  (Open Font Licence)
- Aldrich (Open Font Licence)

Other credits:

- The International Flag of the Planet Earth: Oskar Pernefeldt
- Peace (anti-war) symbol: Gerald Holtom